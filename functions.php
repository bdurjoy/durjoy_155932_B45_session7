<?php

    $num1 = 545;
    $num2 = 955;

    function addition($arg1 , $arg2){
        return ($arg1 + $arg2);
    }

    echo addition($num1, $num2);

    echo "<hr>";

    //recursive function

    $number = 5;

    function factorial($arg){
        echo $arg." ";
            if($arg < 2){
                return 1;
            }
        else {
            return ($arg * factorial($arg - 1));
        }
    }

    echo factorial($number);
?>